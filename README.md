# TASK MANAGER

SCREENSHOTS

https://disk.yandex.ru/d/qmXbFKnnQ8h9Ig?w=1

## DEVELOPER INFO

name: Dmitriy Borisyuk

e-mail: Kytyzoff92work@gmail.com

## HARDWARE

CPU: Ryzen 5 2600

RAM: 16GB

SSD: 512 GB

## SOFTWARE

System: Windows 10 Pro

JDK Version: 1.8.0_281

## PROGRAM RUN

```bash
java -jar ./task-manager.jar
```
 
